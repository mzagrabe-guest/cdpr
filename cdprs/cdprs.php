<?
/*
** cdprs.php - Sample PHP script to collect cdpr data to a file
**
** Copyright (c) 2002-2008 MonkeyMental.com
**
** This program will show you which Cisco device your machine is
** connected to based on CDP packets received.
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 2
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/
	header("Content-type: plain/text");

	$switch = $_GET["switch_ip"];
	$switch6 = $_GET["switch_ip6"];
	$port = $_GET["port"];
	$host = $_GET["host"];
	$loc = $_GET["loc"];

	if(!$fh = fopen("/tmp/cdprs.txt", "a+b"))
	{
		printf("Error opening file\n");
		exit();
	}

	$cdprs = sprintf("Switch: %s/%s Port: $port Host: $host Location: $loc\n", 
					  $switch, $switch6, $port, $host, $loc);

	if(!fwrite($fh, $cdprs))
	{
		printf("Error writing data to file\n");
		fclose($fh);
		exit();
	}
	else
	{
		print("Update Sucessful\n");
	}

?>
